package net.nateyoung.chestdisabler;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Recipe;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Iterator;

public class ChestDisabler extends JavaPlugin {

    @Override
    public void onEnable() {

        PluginManager pm = this.getServer().getPluginManager();

        Iterator<Recipe> it = getServer().recipeIterator();
        Recipe recipe;
        while(it.hasNext()) {
            recipe = it.next();
            if (recipe != null &&
                    (recipe.getResult().getType().equals(Material.CHEST) || recipe.getResult().getType().equals(Material.ENDER_CHEST))
                    ) {
                it.remove();
                this.getLogger().info(ChatColor.RED + recipe.getResult().getType().toString() + " has been removed");
            }
        }


    }




}
